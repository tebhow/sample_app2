require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  # Given .... When .... Then ....
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title",                full_title("Home")
    assert_equal full_title("Home"),      "Home | Sample RoR"
    assert_select "a[href=?]",            signup_path
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title",                full_title("Help")
    assert_equal full_title("Help"),      "Help | Sample RoR"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title",                full_title("About")
    assert_equal full_title("About"),     "About | Sample RoR"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title",                full_title("Contact")
    assert_equal full_title("Contact"),   "Contact | Sample RoR"
  end

end
