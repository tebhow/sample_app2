require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  # Check full_title function delivered correctly or not
  test "full title helper" do
    assert_equal full_title,            "Sample RoR"
    assert_equal full_title("Home"),    "Home | Sample RoR"
    assert_equal full_title("Help"),    "Help | Sample RoR"
    assert_equal full_title("About"),   "About | Sample RoR"
    assert_equal full_title("Contact"), "Contact | Sample RoR"
    assert_equal full_title("Log in"),  "Log in | Sample RoR"
  end
end