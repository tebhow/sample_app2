require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  # User Model Validation test 
  def setup
    @user = User.new( name: "User", email: "user@example.com",
                      password: "foobar78", password_confirmation: "foobar78")
  end

  test "should be valid" do
    assert @user.valid?
    # assert expected true to be true
  end

  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
    # assert_not expected false to be true
  end

  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end

  test "password should be present" do
    @user.password = @user.password_confirmation = " " * 8
    assert_not @user.valid?
  end

  test "name length" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email length" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "password minimum length" do
    @user.password = @user.password_confirmation = "foobar7" # 7 digit while password must be 8 digit to be passed
    assert_not @user.valid?
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "password must combine between letter and number" do
    invalid_passwords = %w[foobaryo FOOBARYO fooBARyO !@#$%^&* foo!@#$%^ FOO!@#$%^&
                          fooBAR!@# 12345678 1234!@#$]
    invalid_passwords.each do |invalid|
      @user.password = @user.password_confirmation = invalid
      assert_not @user.valid?, "#{invalid.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@EXamPlE.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(' ')
  end

end
