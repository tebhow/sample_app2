require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:anugrah)
  end

  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, params: {session: {  email: "", password: ""} }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login with valid information" do
    get login_path
    post login_path, params: {session: {  email: @user.email, password: 'foobar78'} }
    assert is_logged_in?
    assert_redirected_to @user        # After login redirect to user like in session#create action
    follow_redirect!                  # To actually follow and visit to the last response above redirect to @user
    assert_template 'users/show'      # Check if layout users/show appear as the same page with redirect to @user
    assert_select "a[href=?]", login_path, count: 0     # count 0 mean no link appear
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
  end

  test "login with valid information followed by logout" do
    get login_path
    post login_path, params: {session: {  email: @user.email, password: 'foobar78'} }
    assert is_logged_in?
    assert_redirected_to @user        # After login redirect to user like in session#create action
    follow_redirect!                  # To actually follow and visit to the last response above redirect to @user
    assert_template 'users/show'      # Check if layout users/show appear as the same page with redirect to @user
    assert_select "a[href=?]", login_path, count: 0     # count 0 mean no link appear
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # SImulate a user clocking logout in a second window
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_empty cookies['remember_token']
  end

  test "login without remembering" do
    # Log in to set the cookie.
    log_in_as(@user, remember_me: '1')
    # Log in again verify that the cookie is deleted.
    log_in_as(@user, remember_me: '0')
    assert_empty cookies['remember_token']
  end

end
