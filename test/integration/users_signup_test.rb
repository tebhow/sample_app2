require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: {user:  {  name:                   "",
                                          email:                  "user@invalid",
                                          password:               "foo",
                                          password_confirmationn: "bar" } }
      end
      assert_template "users/new"
  end

  test "valid signup information" do
    get signup_path
    assert_difference 'User.count' do
      post users_path, params: {user:  {  name:                   "Exmple User",
                                          email:                  "user@example.com",
                                          password:               "foobar78",
                                          password_confirmationn: "foobar78" } }
      end
      follow_redirect!
      assert_template 'users/show'
      assert_not flash.empty?
      assert is_logged_in?
  end

end
