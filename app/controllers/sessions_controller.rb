class SessionsController < ApplicationController

  def new
    
  end

  def create
    # params from html-post new page
    user = User.find_by(email: params[:session][:email].downcase)
    # condition below check if user inserted by user is valid and exits in database
    # and the check if the password is valid or not with authenticate method
    if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_to user
    else
      # flash.now specifically for displaying flash messages on rendered pages
      flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
      # error message back to new page
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end
